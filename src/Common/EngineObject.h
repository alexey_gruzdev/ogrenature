/**
* @file EngineObject.h
*
* Copyright (c) 2015 by Gruzdev Alexey
*
* Code covered by the MIT License
* The authors make no representations about the suitability of this software
* for any purpose. It is provided "as is" without express or implied warranty.
*/


#ifndef _ENGINE_OBJECT_H_
#define _ENGINE_OBJECT_H_

#include <OgreEntity.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>


class EngineObject
{
    Ogre::Entity* mEntity;
    Ogre::SceneNode* mNode;
    Ogre::SceneManager* mSceneManager;

public:

    EngineObject(Ogre::SceneManager* sceneManager, Ogre::Entity* entity, Ogre::SceneNode* node);

    virtual ~EngineObject();

    const Ogre::SceneNode* GetNode() const
    {
        return mNode;
    }
    Ogre::SceneNode* GetNode()
    {
        return mNode;
    }

    const Ogre::Entity* GetEntity() const
    {
        return mEntity;
    }
    Ogre::Entity* GetEntity()
    {
        return mEntity;
    }
};


template <typename ObjectType = EngineObject>
class EngineObjectsFactory
{
    Ogre::SceneManager* mSceneManager;
public:
    EngineObjectsFactory(Ogre::SceneManager* sceneManager) :
        mSceneManager(sceneManager)
    { }
    ~EngineObjectsFactory()
    { }

    std::unique_ptr<ObjectType> Create(Ogre::Entity* entity)
    {
        return std::make_unique<ObjectType>(mSceneManager, entity, mSceneManager->getRootSceneNode()->createChildSceneNode());
    }

    std::unique_ptr<ObjectType> Create(Ogre::MeshPtr mesh)
    {
        return std::make_unique<ObjectType>(mSceneManager, mSceneManager->createEntity(mesh), mSceneManager->getRootSceneNode()->createChildSceneNode());
    }

    std::unique_ptr<ObjectType> Create(const std::string & meshName)
    {
        return std::make_unique<ObjectType>(mSceneManager, mSceneManager->createEntity(meshName), mSceneManager->getRootSceneNode()->createChildSceneNode());
    }

    std::unique_ptr<ObjectType> Create(Ogre::Entity* entity, Ogre::SceneNode* node)
    {
        return std::make_unique<ObjectType>(mSceneManager, entity, node);
    }

    std::unique_ptr<ObjectType> Create(Ogre::MeshPtr mesh, Ogre::SceneNode* node)
    {
        return std::make_unique<ObjectType>(mSceneManager, mSceneManager->createEntity(mesh), node);
    }

    std::unique_ptr<ObjectType> Create(const std::string & meshName, Ogre::SceneNode* node)
    {
        return std::make_unique<ObjectType>(mSceneManager, mSceneManager->createEntity(meshName), node);
    }
};


#endif