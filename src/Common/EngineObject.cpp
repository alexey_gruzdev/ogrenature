/**
* @file EngineObject.cpp
*
* Copyright (c) 2015 by Gruzdev Alexey
*
* Code covered by the MIT License
* The authors make no representations about the suitability of this software
* for any purpose. It is provided "as is" without express or implied warranty.
*/

#include "EngineObject.h"

#include <OgreSceneManager.h>

EngineObject::EngineObject(Ogre::SceneManager* sceneManager, Ogre::Entity* entity, Ogre::SceneNode* node) :
mEntity(entity), mNode(node), mSceneManager(sceneManager)
{
    mNode->attachObject(mEntity);
}

EngineObject::~EngineObject()
{
    mNode->detachAllObjects();
    mSceneManager->destroyEntity(mEntity);
    mSceneManager->destroySceneNode(mNode);
}