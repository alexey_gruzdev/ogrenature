/**
* @file EternalForest.cpp
*
* Copyright (c) 2015 by Gruzdev Alexey
*
* Code covered by the MIT License
* The authors make no representations about the suitability of this software
* for any purpose. It is provided "as is" without express or implied warranty.
*/


#include "EternalForest.h"

#include <boost/multi_array.hpp>

#include <OgreSceneManager.h>
#include <OgreEntity.h>
#include <OgreSceneNode.h>
#include <OgreImage.h>
#include <OgreTexture.h>
#include <OgreTextureManager.h>
#include <OgreMaterial.h>
#include <OgreMaterialManager.h>
#include <OgreTechnique.h>
#include <OgrePass.h>
#include <OgreHighLevelGpuProgram.h>
#include <OgreHighLevelGpuProgramManager.h>

#include "Ground.h"
#include "World.h"


namespace
{
    static const char Shader_GL_Tree_V[] = ""
        "#version 120                                                              \n"
        "                                                                          \n"
        "                                                                          \n"
        "void main()                                                               \n"
        "{                                                                         \n"
        "    gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;             \n"
        "    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;               \n"
        "}                                                                         \n"
        "";


    static const char Shader_GL_Tree_F[] = ""
        "#version 120                                                                \n"
        "                                                                            \n"
        "uniform sampler2D texture;                                                  \n"
        "                                                                            \n"
        "void main()                                                                 \n"
        "{                                                                           \n"
        "    vec4 color = texture2D(texture, gl_TexCoord[0].st);                     \n"
        "    if(color.a < 0.00001) discard;                                          \n"
        "    gl_FragColor = color;                                                   \n"
        "}                                                                           \n"
        "";
}


//-------------------------------------------------------
// Tree

Ogre::Material* Tree::CreareTreeMaterail(const std::string & name, TreeType type)
{
    Ogre::Image image;
    image.load("tree_18_h.png", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    Ogre::TexturePtr texture = Ogre::TextureManager::getSingleton().loadImage("Texture/Tree/" + name,
        Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, image);

    Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create(name, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    {
        Ogre::Technique* techniqueGL = material->getTechnique(0);
        Ogre::Pass* pass = techniqueGL->getPass(0);
        {
            auto vprogram = Ogre::HighLevelGpuProgramManager::getSingleton().createProgram("Shader/Tree/GL/Textured/V",
                Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, "glsl", Ogre::GPT_VERTEX_PROGRAM);
            vprogram->setSource(Shader_GL_Tree_V);

            pass->setVertexProgram(vprogram->getName());
        }
        {
            auto fprogram = Ogre::HighLevelGpuProgramManager::getSingleton().createProgram("Shader/Tree/GL/Textured/F",
                Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, "glsl", Ogre::GPT_FRAGMENT_PROGRAM);
            fprogram->setSource(Shader_GL_Tree_F);


            auto unit = pass->createTextureUnitState(texture->getName());
            unit->setTextureAddressingMode(Ogre::TextureUnitState::TAM_WRAP);
            unit->setTextureFiltering(Ogre::TFO_ANISOTROPIC);

            pass->setFragmentProgram(fprogram->getName());

            auto fparams = pass->getFragmentProgramParameters();
            fparams->setNamedConstant("texture", 0);

        }
        techniqueGL->setSceneBlending(Ogre::SBT_TRANSPARENT_ALPHA);
        techniqueGL->setDepthCheckEnabled(true);
        techniqueGL->setDepthFunction(Ogre::CMPF_LESS_EQUAL);
        techniqueGL->setDepthWriteEnabled(true);
    }
#if !NDEBUG
    material->load();
#endif
    return material.get();
}


//-------------------------------------------------------
//Eternal Forest

const float EternalForest::FIELD_BLOCK_SIZE  = 1.0f;
const float EternalForest::FIELD_UPDATE_TICK = 1.0f;
//-------------------------------------------------------
EternalForest::EternalForest(Ogre::SceneManager* sceneManager, const World* world, const Ground* ground, const Ogre::AxisAlignedBox & forestBorders):
    mBorders(forestBorders), mSceneManager(sceneManager), mGround(ground), mWorld(world), mUpdateTickController(FIELD_UPDATE_TICK)
{
    
}
//-------------------------------------------------------
EternalForest::~EternalForest()
{

}
//-------------------------------------------------------
void EternalForest::InitField(size_t startAmount)
{
    Ogre::Vector3 minBorder = mBorders.getMinimum();
    Ogre::Vector3 maxBorder = mBorders.getMaximum();

    uint32_t fieldSizeX = static_cast<uint32_t>((maxBorder[0] - minBorder[0]) / FIELD_BLOCK_SIZE);
    uint32_t fieldSizeZ = static_cast<uint32_t>((maxBorder[2] - minBorder[2]) / FIELD_BLOCK_SIZE);

    mFieldOffset[0] = 0.5f * std::fmod(maxBorder[0] - minBorder[0], FIELD_BLOCK_SIZE) + minBorder[0];
    mFieldOffset[1] = 0.5f * std::fmod(maxBorder[0] - minBorder[0], FIELD_BLOCK_SIZE) + minBorder[2];

    mLifeField = std::make_unique<LifeField>(boost::extents[fieldSizeZ][fieldSizeX]);
    mLifeFieldNext = std::make_unique<LifeField>(boost::extents[fieldSizeZ][fieldSizeX]);
    
    LifeField& field = *mLifeField;
    for (uint32_t z = 0; z < fieldSizeZ; ++z)
    {
        for (uint32_t x = 0; x < fieldSizeX; ++x)
        {
            if (z == 0 || z == fieldSizeZ - 1 || x == 0 || x == fieldSizeX - 1)
            {
                field[z][x].flags = BlockInfo::BLOCKED;
                continue;
            }
            float s = mFieldOffset[0] + (x + 0.5f) * FIELD_BLOCK_SIZE;
            float t = mFieldOffset[1] + (z + 0.5f) * FIELD_BLOCK_SIZE;
            float h = mWorld->GetGroundHeightAt(s, t);
            field[z][x].flags = (h >= minBorder[1] && h <= maxBorder[1]) ? BlockInfo::EMPTY : BlockInfo::BLOCKED;
            field[z][x]._height = h;
        }
    }

    //generate random start positions
    size_t amount = std::min(startAmount, fieldSizeX * fieldSizeZ);
    while (amount > 0)
    {
        uint8_t attempts = 0;
        while (attempts < 10)
        {
            uint32_t x = static_cast<uint32_t>(Ogre::Math::UnitRandom() * fieldSizeX);
            uint32_t z = static_cast<uint32_t>(Ogre::Math::UnitRandom() * fieldSizeZ);
            if ((*mLifeField)[z][x].flags & BlockInfo::EMPTY)
            {
                (*mLifeField)[z][x].flags = BlockInfo::TREE;
                auto tree = mSceneManager->createEntity("tree_1.mesh");
                auto node = mSceneManager->getRootSceneNode()->createChildSceneNode();
                node->setScale(0.0005f, 0.0005f, 0.0005f);
                node->setPosition(Ogre::Vector3(mFieldOffset[0] + (x + 0.5f) * FIELD_BLOCK_SIZE, (*mLifeField)[z][x]._height, mFieldOffset[1] + (z + 0.5f) * FIELD_BLOCK_SIZE));
                node->attachObject(tree);
                (*mLifeField)[z][x].treeNode = node;
                break;
            }
            ++attempts;
        }
        --amount;
    }
}
//-------------------------------------------------------
void EternalForest::UpdateField(float time, size_t quota)
{
    LifeField& field = *mLifeField;
    uint32_t width =  mLifeField->shape()[0];
    uint32_t height = mLifeField->shape()[1];

    for (uint32_t z = 1; z < height - 1; ++z)
    {
        for (uint32_t x = 1; x < width - 1; ++x)
        {
            BlockInfo& current = field[z][x];
            BlockInfo& next = (*mLifeFieldNext)[z][x];
            assert(current.flags == BlockInfo::BLOCKED || current.flags == BlockInfo::TREE || current.flags == BlockInfo::EMPTY);

            next = current;
            if (0 == (current.flags & BlockInfo::BLOCKED))
            {
                auto neighbors = {
                    field[z - 1][x - 1].flags,
                    field[z - 1][x].flags,
                    field[z - 1][x + 1].flags,
                    field[z][x - 1].flags,
                    field[z][x + 1].flags,
                    field[z + 1][x - 1].flags,
                    field[z + 1][x].flags,
                    field[z + 1][x + 1].flags
                };
                uint8_t sum = 0;
                std::for_each(std::cbegin(neighbors), std::cend(neighbors), [&sum](const uint8_t& flags)->void { if(flags & BlockInfo::TREE) ++sum; });
                if (current.flags & BlockInfo::TREE)
                {
                    if (sum < 3 || sum > 4)
                    {
                        current.treeNode->detachAllObjects();
                        current.treeNode->removeAndDestroyAllChildren();
                        current.treeNode = nullptr;

                        next.flags = BlockInfo::EMPTY;
                        next.treeNode = nullptr;
                    }
                }
                else if (current.flags & BlockInfo::EMPTY)
                {
                    if (sum >= 3 && sum <= 4)
                    {
                        auto tree = mSceneManager->createEntity("tree_1.mesh");
                        auto node = mSceneManager->getRootSceneNode()->createChildSceneNode();
                        node->setScale(0.0005f, 0.0005f, 0.0005f);
                        node->setPosition(Ogre::Vector3(mFieldOffset[0] + (x + 0.5f) * FIELD_BLOCK_SIZE, (*mLifeField)[z][x]._height, mFieldOffset[1] + (z + 0.5f) * FIELD_BLOCK_SIZE));
                        node->attachObject(tree);

                        next.flags = BlockInfo::TREE;
                        next.treeNode = node;
                    }
                }
            }
        }
    }
    mLifeField.swap(mLifeFieldNext);
}
//-------------------------------------------------------
void EternalForest::Update(float time)
{
    if (nullptr == mLifeField.get())
    {
        InitField(3 * mTreesQuota);
    }
    else
    {
        if (mUpdateTickController.Tick(time))
        {
            UpdateField(time, mTreesQuota);
        }
    }
}